# My Gitbook

Some random words

* [Concourse](https://blog.ruanbekker.com/blog/categories/concourse/)
    * [Concourse Pipelines with Docker](posts/build-concourse-pipeline-docker-image-on-git-commit.md)
    * [GitBook is nice](part1/gitbook.md)
* [Python](https://blog.ruanbekker.com/blog/categories/python)
    * [Caching with Flask](posts/2019-02-14-how-to-cache-data-with-python-flask.md)
* [Elastic](part2/better_tools.md)
    * [Ship Logs to Filebeat](posts/2019-03-27-ship-your-logs-to-elasticsearch-with-filebeat.md)
